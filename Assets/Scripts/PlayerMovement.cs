using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 20f;
    private Vector3 _movement;
    private Rigidbody _rb;
    private Animator _animator;
    private static readonly int SpeedF = Animator.StringToHash("Speed_f");

    private Camera _camera;
    
    private void Start()
    {
        _camera = Camera.main;
        _animator = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody>();
    }
    
    private void Update()
    {
        if (Time.timeScale == 0) return;
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        _movement.Set(horizontal, 0f, vertical);
        _movement.Normalize();

        _animator.SetFloat(SpeedF,_movement.magnitude);

        _movement *= moveSpeed;
        Vector3 velocity = Vector3.ProjectOnPlane(_movement, Vector3.zero);

        _rb.velocity = velocity;

        RotateTowardsMouse();
    }

    private void RotateTowardsMouse()
    {
        Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);

        if (groundPlane.Raycast(ray, out float rayDistance))
        {
            Vector3 point = ray.GetPoint(rayDistance);
            point.y = transform.position.y;
            transform.LookAt(point);
        }
    }
}