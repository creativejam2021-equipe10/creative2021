using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 10;
    [SerializeField] private float turnSpeed = 10;
    [SerializeField] private float detectionRange = 30;
    [SerializeField] private float damageAmount = 5;

    [SerializeField] private SkinnedMeshRenderer[] models;
    [SerializeField] private Material[] materials;
    
    private Health _player;
    private NavMeshAgent _agent;
    private Animator _animator;
    private Health _health;
    private static readonly int Attack = Animator.StringToHash("Attack");
    private static readonly int SpeedF = Animator.StringToHash("Speed_f");
    private static readonly int Dead = Animator.StringToHash("Dead");

    private const float MinDistanceToPlayer = 3f;
    private bool _dead;

    private ParticleSystem _bloodEffect;

    private void Start()
    {
        _bloodEffect = transform.GetComponentInChildren<ParticleSystem>();
        int modelNumber = Random.Range(0, models.Length);
        models[modelNumber].gameObject.SetActive(true);
        models[modelNumber].sharedMaterial = materials[modelNumber];
        
        _player = GameObject.FindWithTag("Player").GetComponent<Health>();
        _health = GetComponent<Health>();
        _health.OnDeath += OnDeath;
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponentInChildren<Animator>();
        _agent.speed = moveSpeed;
        _agent.angularSpeed = turnSpeed;
        _agent.stoppingDistance = 2.5f;
        _agent.isStopped = true;
        
        InvokeRepeating(nameof(UpdateTarget),0f,.75f);

    }

    private void Update()
    {
        if (!_dead && _agent)
        {
            if (CloseToPlayer())
            {
                _agent.isStopped = true;
                _animator.SetBool(Attack, true);
            }
            else
            {
                _agent.isStopped = false;
                _animator.SetFloat(SpeedF, _agent.velocity.magnitude);
                _animator.SetBool(Attack, false);
            }
        }
    }

    private bool CloseToPlayer() => Vector3.Distance(transform.position, _player.transform.position) <= MinDistanceToPlayer;

    private void OnDeath()
    {
        _dead = true;
        CancelInvoke(nameof(UpdateTarget));
        Destroy(_agent);
        _animator.SetBool(Dead,true);
        StartCoroutine(FadeAndDestroy());
    }

    private IEnumerator FadeAndDestroy()
    {
        yield return new WaitForSeconds(1.5f);
        while (transform.position.y > -1.5f)
        {
            transform.Translate(Vector3.down * Time.deltaTime);
            yield return null;
        }
        
        Destroy(gameObject);
    }

    [UsedImplicitly]
    public void AttackPlayer()
    {
        if (CloseToPlayer())
        {
            _player.TakeDamage(damageAmount);
        }
    }

    private void UpdateTarget()
    {
        if (Vector3.Distance(transform.position, _player.transform.position) < detectionRange)
        {
            _agent.isStopped = false;
            _agent.SetDestination(_player.transform.position);
        }
        else
        {
            _agent.isStopped = true;
        }
    }


    public void Stagger() 
    {
        _bloodEffect.Play();
        if (_agent) 
        {
            transform.position -= transform.forward * 0.5f;
            _agent.velocity = new Vector3(0f, 0f, 0f);
        }
    }

    public void TakeDamage(float damageTaken)
    {
        _health.TakeDamage(damageTaken);
    }



#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,detectionRange);
    }
#endif
}
