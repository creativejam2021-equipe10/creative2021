using UnityEngine;

public class SpawnZombie : MonoBehaviour
{
    [SerializeField] private GameObject _spawnObject;
    [SerializeField] private BoxCollider _collider;

    [SerializeField] private int _maxZombieSpawn;
    public float RateOfSpawn = 1;
    private float nextSpawn;

    private int _nbZombieSpawn = 0;
    
    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextSpawn && _nbZombieSpawn < _maxZombieSpawn)
        {
            nextSpawn = Time.time + RateOfSpawn;
           
            // Random position within this transform
            Vector3 rndPosWithin;
            rndPosWithin = new Vector3(Random.Range(-1f, 1f) * _collider.size.x, Random.Range(-1f, 1f), Random.Range(-1f, 1f)* _collider.size.z);
            rndPosWithin = transform.TransformPoint(rndPosWithin * .5f);
            Instantiate(_spawnObject, rndPosWithin, transform.rotation); 

            _nbZombieSpawn++;     
        }
    }
}
