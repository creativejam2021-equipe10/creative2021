﻿using System;

[Serializable]
public struct RangedFloat
{
    public float minValue;
    public float maxValue;

    public float RandomValueInRange()
    {
        return UnityEngine.Random.Range(minValue, maxValue);
    }
}