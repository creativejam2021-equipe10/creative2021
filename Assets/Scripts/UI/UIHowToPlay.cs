using UnityEngine;

public class UIHowToPlay : MonoBehaviour
{
    [SerializeField] private GameObject _howToPlayPanel;

    [SerializeField] private GameObject _howToPlayPanel2;
    [SerializeField] private GameObject _settingPanel;

    [SerializeField] private GameObject _mainPanel;
  
    public void OnClickBack()
    {
        _howToPlayPanel2.SetActive(false);
        _howToPlayPanel.SetActive(true);
    }
    public void OnClickNext()
    {
        _howToPlayPanel.SetActive(false);
        _howToPlayPanel2.SetActive(true);
    }

    public void OnClickReturn()
    {
        _mainPanel.SetActive(false);
        _settingPanel.SetActive(true);
    }
}
