using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class UIMainMenu : MonoBehaviour
    {

        [SerializeField] private GameObject mainMenu;
        [SerializeField] private GameObject howToPlay;

        [SerializeField] private GameObject howToPlay2;

        [SerializeField] private AudioSource click;
    
        public void OnPlayGameClicked()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    
        public void OnHowToPlayClicked()
        {
            mainMenu.SetActive(false);
            howToPlay.SetActive(true);
        }
        public void OnBackClicked()
        {
            howToPlay.SetActive(true);
            howToPlay2.SetActive(false);
        }

        public void OnNextClicked()
        {
            howToPlay.SetActive(false);
            howToPlay2.SetActive(true);
        }

        public void OnMainMenuClicked()
        {
            howToPlay.SetActive(false);
            mainMenu.SetActive(true);
        }
    
        public void OnQuitGameClicked()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
    }
}
