using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIActionScript : MonoBehaviour
    {

        [SerializeField] private Interactor _interactor;
        
        [SerializeField] private Slider _slider;

        [SerializeField] private GameObject _canvas;

        [SerializeField] private TextMeshProUGUI _text;

        private void OnEnable()
        {
            _interactor.OnInteractableDetection += OnInteractableDetection;
            _interactor.OnInteractionUpdateProgress += OnInteractableProgressUpdate;
        }

        private void OnDisable()
        {
            _interactor.OnInteractableDetection -= OnInteractableDetection ;
            _interactor.OnInteractionUpdateProgress -= OnInteractableProgressUpdate;
        }

        private void OnInteractableDetection(bool detected)
        {
            _canvas.SetActive(detected);
        }

        private void OnInteractableProgressUpdate(Restoration obj)
        {
            _text.text = obj.IsHold
                ? $"HOLD {obj.GetHoldTime()} seconds"
                : $"TAP {obj.GetNumberTap()} times";
            _slider.value = obj.GetRepairRatio();
        }
        
        private void LateUpdate()
        {
            transform.localEulerAngles = Vector3.Scale(transform.parent.eulerAngles,Vector3.up) * -1;
        }

    }
}
