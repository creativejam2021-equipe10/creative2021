using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIZombieHealthBar : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Health _health;

    public UnityAction<float> OnZombieDamage;
    // Start is called before the first frame update
    private void OnEnable() => _health.OnTakeDamage += OnDamageZombie;
    
    private void OnDisable() => _health.OnTakeDamage -= OnDamageZombie;

    private void LateUpdate() => transform.localEulerAngles = Vector3.Scale(transform.parent.eulerAngles,Vector3.up) * -1;

    private void OnDamageZombie(float value) => _slider.value = value;
}
