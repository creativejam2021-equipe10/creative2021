using UnityEngine;
using UnityEngine.SceneManagement;

public class UIVictoryScreen : MonoBehaviour
{
    [SerializeField] private GameObject _panelVictory;
    [SerializeField] private GameManager _gameManager;
    
    private void OnEnable() => _gameManager.OnLevelComplete += OnLevelComplete;

    private void OnDisable() => _gameManager.OnLevelComplete -= OnLevelComplete;


    public void OnClickNextLevel()
    {
        SceneManager.LoadScene(1);
    }

    public void OnClickExit()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
        _panelVictory.SetActive(false);
    }

    private void OnLevelComplete()
    {
        Time.timeScale = 0;
        _panelVictory.SetActive(true);
    }
}
