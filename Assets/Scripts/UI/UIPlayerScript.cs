using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UI
{
    public class UIPlayerScript : MonoBehaviour
    {
        [SerializeField] private Slider _healthBar;
        [SerializeField] private Health _player;

        [SerializeField] private GameManager _gameManager;

        [SerializeField] private GameObject _pnlEvent;
        [SerializeField] private TextMeshProUGUI _eventText;

        private bool isEvent;
        private float timeLeft;
        private void Update() 
        {
            if (isEvent)
            {
                if (timeLeft >= 0)
                {
                    timeLeft -= Time.deltaTime;
                }
                else
                {
                    timeLeft = 0;
                    isEvent = false;
                    _pnlEvent.SetActive(false);
                }
            }
        }
        private void OnEnable()
        {
            _player.OnTakeDamage += OnHealthChanged;
            _gameManager.OnSectionComplete += OnSectionComplete;
            isEvent = false;
        }

        private void OnDisable()
        {
            _player.OnTakeDamage -= OnHealthChanged;
            _gameManager.OnSectionComplete -= OnSectionComplete;
        }

        private void OnHealthChanged (float value)
        {
            _healthBar.value = value;
        }

        private void OnSectionComplete(string message)
        {
            _eventText.text = message;
            _pnlEvent.SetActive(true);
            isEvent = true;
            timeLeft = 5f;
        }
    }
}
