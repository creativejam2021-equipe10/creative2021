using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class UISettingScript : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        private GameObject _settingPanel;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            _settingPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
