using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class UISettingPanel : MonoBehaviour
    {
        [SerializeField] private GameObject _howToPlayPanel;
        public void OnClickResume()
        {
            Time.timeScale = 1;
            transform.parent.gameObject.SetActive(false);
        }

        public void OnHowToClick()
        {
            _howToPlayPanel.SetActive(true);
            transform.parent.gameObject.SetActive(false);
        }

        public void OnQuitClick()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
            transform.parent.gameObject.SetActive(false);
        }
    }
}
