using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class UIGameOver : MonoBehaviour
    {
        [SerializeField] private Health _health;
        [SerializeField] private UITimer _timer;
        [SerializeField] private GameObject _panelGameOver;
    
        void OnEnable()
        {
            _health.OnDeath += OnPlayerDeath;
            _timer.OnTimeOver += OnPlayerDeath;
        }

        // Update is called once per frame
        void OnDisable()
        {
            _health.OnDeath -= OnPlayerDeath;
            _timer.OnTimeOver -= OnPlayerDeath;
        }
        public void OnClickContinue()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(1);
            _panelGameOver.SetActive(false);
        }

        public void OnClickQuit()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
            _panelGameOver.SetActive(false);
        }

        private void OnPlayerDeath()
        {
            Time.timeScale = 0;
            _panelGameOver.SetActive(true);
        }
    }
}
