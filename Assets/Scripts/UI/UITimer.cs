using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class UITimer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    public float timeRemaining = 10;
    public bool timerIsRunning;
    public UnityAction OnTimeOver;
    private void Start()
    {
        timerIsRunning = true;
    }

    private void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                timeRemaining = 0;
                timerIsRunning = false;
                OnTimeOver?.Invoke();
                DisplayTime(0);
            }
        }
    }
    private void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60); 
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

         _text.text = $"{minutes:00}:{seconds:00}";
    }
}
