using System.Collections;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField] private float damageAmount = 25;
    [SerializeField] private float shotsDelay = 0.05f;
    [SerializeField] private float range = 10f;
    [SerializeField] private Transform gunPoint;
    [SerializeField] private ParticleSystem shootEffect;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private LineRenderer lineRenderer;
    
    private bool _canShoot = true;
    private Animator _animator;
    private static readonly int ShootB = Animator.StringToHash("Shoot_b");
    private Vector3 _targetDirection;
    
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }
    
    private void Update()
    {
        if (Time.timeScale == 0) return;
        _targetDirection = transform.forward * range + gunPoint.position;
        UpdateLineRenderer();
        

        if (_canShoot && Input.GetMouseButton(0))
        {
            Shoot();
        }
    }

    private IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(shotsDelay);
        _animator.SetBool(ShootB, false);
        _canShoot = true;
    }


    private void Shoot()
    {
        _canShoot = false;
        _animator.SetBool(ShootB, true);
        shootEffect.Play();
        audioSource.Play();
        if (Physics.Raycast(gunPoint.position, transform.forward, out RaycastHit hit, range))
        {
            if (hit.transform.CompareTag("Zombie"))
            {
                Zombie zombie = hit.transform.GetComponent<Zombie>();
                zombie.Stagger();
                zombie.TakeDamage(damageAmount);
            }
        }
        StartCoroutine(ShootDelay());
    }


    private void UpdateLineRenderer()
    {
        lineRenderer.SetPosition(0, gunPoint.position);
        lineRenderer.SetPosition(1, _targetDirection);
    }
}
