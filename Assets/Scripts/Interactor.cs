using UnityEngine;
using UnityEngine.Events;

public class Interactor : MonoBehaviour
{
    public UnityAction<bool> OnInteractableDetection;
    public UnityAction<Restoration> OnInteractionUpdateProgress;
    
    
    [SerializeField] private LayerMask mask;
    [SerializeField] private float interactionDistance;

    private Restoration _closestRestoration;

    private void Start()
    {
        InvokeRepeating(nameof(DetectInteractable),0,0.1f);
    }

    private void Update()
    {
        if (_closestRestoration){
            if (_closestRestoration.IsHold)
            {
                if (Input.GetKey(KeyCode.E))
                {
                    _closestRestoration.IncreaseProgress();
                    OnInteractionUpdateProgress.Invoke(_closestRestoration);
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    _closestRestoration.IncreaseProgress();
                    OnInteractionUpdateProgress.Invoke(_closestRestoration);
                }
            }
        }
    }

    private void DetectInteractable()
    {
        Collider[] _colliders = Physics.OverlapSphere(transform.position, interactionDistance, mask);
        float minDistance = float.MaxValue;
        _closestRestoration = null;
        Collider closestCollider = null;
        foreach (Collider col in _colliders)
        {
            float distance = Vector3.Distance(transform.position, col.transform.position);
            if (distance < minDistance)
            {
                closestCollider = col;
            }
        }

        if (closestCollider)
        {
            _closestRestoration = closestCollider.GetComponentInParent<Restoration>();
            OnInteractionUpdateProgress.Invoke(_closestRestoration);    
        }
        
        OnInteractableDetection.Invoke(closestCollider && !_closestRestoration.IsDone);
        
    }


#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position,interactionDistance);
    }
#endif
}
