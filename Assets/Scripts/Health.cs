using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public UnityAction OnDeath;
    public UnityAction<float> OnTakeDamage;
    
    [SerializeField] private float MaxHealth = 100;

    [SerializeField] private AudioSource _deathSound;

    private float _currentHealth;

    private void Start()
    {
        _currentHealth = MaxHealth;
    }

    public void TakeDamage(float damageAmount)
    {
        _currentHealth = Mathf.Max(_currentHealth - damageAmount,0);
        OnTakeDamage?.Invoke(_currentHealth/MaxHealth);

        if (_currentHealth == 0)
        {
            OnDeath?.Invoke();
            _deathSound.Play();
        }
    }
}
