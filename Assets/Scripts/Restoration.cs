using System.Collections;
using UnityEngine;

public class Restoration : MonoBehaviour
{

    [Header("References")]
    [SerializeField] private GameObject damagedObject;
    [SerializeField] private GameObject restoredObject;

    
    [Header("Configuration")]
    [SerializeField] private float maxNumberOfHoldTime;
    [SerializeField] private int maxNumberOfTaps;
    [SerializeField] private bool _isHoldAction;
    [SerializeField] private AudioSource _completeSound;

    public bool IsDone { get; private set; }
    public bool IsHold => _isHoldAction;

    private GameManager _gameManager;
    private int _numberOfTaps;
    private float _holdTime;

    private int _numberOfTapsLeft;

    private float _holdTimeLeft;
    private static readonly int CutoffHeight = Shader.PropertyToID("_CutoffHeight");
    

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
        IsDone = false;
        UpdateModel();
    }

    private void Restore()
    {
        if (!IsDone)
        {
            IsDone = true;
            Destroy(damagedObject.GetComponent<Outline>());
            StartCoroutine(TransitionToNewModel());
            _completeSound.Play();
        }
    }

    private IEnumerator TransitionToNewModel()
    {
        Material dmgmat = damagedObject.GetComponent<Renderer>().materials[1];
        Renderer[] dmgmats = damagedObject.GetComponentsInChildren<Renderer>();
        Material resmat = restoredObject.GetComponent<Renderer>().materials[1];
        Renderer[] resmats = restoredObject.GetComponentsInChildren<Renderer>();
        resmat.SetFloat(CutoffHeight,17);
        float test = -5;
        while (test < 17)
        {
            dmgmat.SetFloat(CutoffHeight,test);
            foreach (Renderer dmgmat1 in dmgmats)
            {
                dmgmat1.materials[1].SetFloat(CutoffHeight,test);
            }
            test += Time.deltaTime * 20;
            yield return null;
        }
        foreach (Renderer dmgmat1 in dmgmats)
        {
            dmgmat1.material = dmgmat;
        }
        restoredObject.SetActive(true);
        //UpdateModel();
        while (test > -5)
        {
            resmat.SetFloat(CutoffHeight,test);
            dmgmat.SetFloat(CutoffHeight,test);
            foreach (Renderer resmat1 in resmats)
            {
                resmat1.materials[1].SetFloat(CutoffHeight,test);
            }
            foreach (Renderer dmgmat1 in dmgmats)
            {
                dmgmat1.materials[0].SetFloat(CutoffHeight,test);
                dmgmat1.materials[1].SetFloat(CutoffHeight,test);
            }
            test -= Time.deltaTime * 20;
            yield return null;
        }
        damagedObject.SetActive(false);
        _gameManager.BuildingComplete();    
    }

    public void IncreaseProgress()
    {
        if (_isHoldAction)
        {
            _holdTime += Time.deltaTime;
        }
        else
        {
            _numberOfTaps++;
        }

        if (GetRepairRatio() >= 1)
        {
            Restore();
        }
    }

    private void UpdateModel()
    {
        damagedObject.SetActive(!IsDone);
        restoredObject.SetActive(IsDone);
    }
    
    public float GetRepairRatio() => _isHoldAction ? _holdTime / maxNumberOfHoldTime : (float) _numberOfTaps / maxNumberOfTaps;
    public double GetHoldTime() => System.Math.Round(maxNumberOfHoldTime - _holdTime,2);
    public float GetNumberTap() => maxNumberOfTaps - _numberOfTaps;
}
