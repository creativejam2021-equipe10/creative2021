using UnityEngine;

public class Dancer : MonoBehaviour
{
    private static readonly int AnimationINT = Animator.StringToHash("Animation_int");
    private static readonly int WeaponTypeINT = Animator.StringToHash("WeaponType_int");

    private void Start()
    {
        Animator animator = GetComponent<Animator>();
        animator.SetInteger(AnimationINT,4);
        animator.SetInteger(WeaponTypeINT,0);
        animator.speed = Random.Range(0.5f, 1.5f);
    }
}
