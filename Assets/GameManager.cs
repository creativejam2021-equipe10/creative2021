using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public UnityAction OnLevelComplete;
    public UnityAction<string> OnSectionComplete;
    
    [SerializeField] private int _nbBuilding;

    [SerializeField] private GameObject _levelOneBlocker;
    [SerializeField] private GameObject _levelTwoBlocker;
    [SerializeField] private GameObject _levelThreeBlocker;

    private int _sectionOneBuilding = 5;
    private int _sectionTwoBuilding = 9;
    private int _sectionThreeBuilding = 11;

    private int _buildingComplete;

    private bool section1Complete = false;
    private bool section2Complete = false;
    private bool section3Complete = false;

    public void BuildingComplete()
    {
        _buildingComplete++;
        if (_buildingComplete >= _nbBuilding)
        {
            OnLevelComplete?.Invoke();
        }

        else if (_buildingComplete >= _sectionThreeBuilding)
        {
            if (!section3Complete)
            {
                OnSectionComplete.Invoke("The Mall is Open");
                _levelThreeBlocker.SetActive(false);
                section3Complete = true;
            }
        }
        else if( _buildingComplete >= _sectionTwoBuilding)
        {
            if(!section2Complete)
            {
                OnSectionComplete.Invoke("The Thunder Dome is Open");
                _levelTwoBlocker.SetActive(false);
                section2Complete = true;
            }

        }
        else if (_buildingComplete >= _sectionOneBuilding)
        {
            if (!section1Complete)
            {
                OnSectionComplete?.Invoke("The forest zone is Open");
                _levelOneBlocker.SetActive(false);
                section1Complete = true;
            }
        }
    }
}
